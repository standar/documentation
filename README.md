# Documentation des services de la TGIR Huma-Num

Cette documentation en édition continue est régulièrement mise à jour. La version en cours (branche `master`) est consultable à l'adresse [documentation.huma-num.fr](https://documentation.huma-num.fr).

La documentation est un outil essentiel de l'offre de service Huma-Num. Nous avons fait le choix de rendre ce répertoire le plus ouvert possible à la contribution. En tant qu'usagers des services Huma-Num, vos retours sont précieux, n'hésitez pas à y contribuer en apportant des précisions, des mises-à-jour ou toute correction pertinente.

## Contribuer

Plusieurs manières pour contribuer à l'amélioration de la documentation :

### 1. Vous identifiez un problème (un manque, une faute d'orthographe, un lien cassé, etc.)

1. ouvrez un ticket [[New issue]](https://gitlab.huma-num.fr/huma-num-public/documentation/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
2. décrivez le problème succintement.
3. éventuellement caractérisez le problème à l'aide d'un _label_.

### 2. Vous avez des éléments à ajouter ou une édition à faire

**Première possibilité &rarr; vous êtes pressé :**

1. ouvrez une issue [[New issue]](https://gitlab.huma-num.fr/huma-num-public/documentation/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
2. rédigez le·s paragraphe·s à intégrer
3. éventuellement donnez des instructions pour l'intégration des paragraphes.

**Deuxième possibilité &rarr; vous avez un peu de temps :**

1. dans [le répertoire `docs/`](https://gitlab.huma-num.fr/huma-num-public/documentation/-/tree/master/docs) identifiez et cliquez sur le fichier markdown à éditer,
3. cliquez sur le bouton bleu [Edit],
4. éditez le fichier. Pour "enregistrer" vos modifications, veillez à:
   1. documentez votre modification dans le champ `Commit message`
   2. nommez votre changement dans le champ `Target Branch`
     (explication: vous êtes confiant, vous pouvez choisir la branche `master`, les modifications seront intégrées immédiatement. Vous souhaitez avoir une validation, sur le fond ou sur la forme, dans ce cas "nommez une nouvelle branche" afin de générer un ticket de validation qui sera assigné à l'un des éditeurs de la documentation).
   3. cliquez sur le bouton `Commit changes`

### 3. Vous voulez participer au maintien de la documentation

_prérequis : quelques notions du protocole git._

Cette documentation utilise [MkDocs](https://www.mkdocs.org/). Son installation n'est nécessaire que si vous souhaitez prévisualiser vos modifications en local.

**Pour éditer la documentation en local :**

- `git clone` pour une première installation
- `git pull` avant chaque modification
- Pour changer les sources (Markdown)
    - éditez les fichiers markdown dans le répertoire `docs`, sauvegardez, puis :
  - `git add ./docs/*.md` pour prendre en compte tous les fichiers `.md`
  - `git commit -m "description de la mise à jour"` pour valider et documenter les modifications
  - `git push` pour envoyer vers le serveur (gitlab)

IMPORTANT : Cette documentation est indexée par [ISIDORE](https://isidore.science). Ainsi, les fichiers markdown du répertoire `docs` doivent contenir des métadonnées sous la forme:

```markdown
---
lang: fr [ou tout code langue: fr, fre, en, eng, etc.]
description: >-
  Une description dans la langue de la page 
  qui peut être sur plusieurs lignes avec tous 
  les signes de ponctuation classique !
tags: "mot-clé 1, mot-clé 2, etc."
---
```




**Pour prévisualiser la documentation en local :**

  - installez les [requirements](requirements.txt)
  - installez mkdocs : `pip install mkdocs` ([voir la documentation](https://www.mkdocs.org/#installation))
  - dans le répertoire cloné, lancez la commande `mkdocs serve` pour générer la documentation et la servir par défaut sur `http://127.0.0.1:8000`
