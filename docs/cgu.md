---
lang: fr
---

# Conditions générales d’utilisation (CGU)

*Version en cours.
*

Huma-Num propose différentes solutions adaptées pour entreposer et organiser les données numériques (ou des documents) dans un espace sécurisé professionnel, offrant une haute disponibilité d’accès. Ce service est opéré par Huma-Num sur son propre serveur de fichiers (NAS) et en coopération avec le CC-IN2P3/CNRS pour le service iRods.

Cet article a pour objet de définir les règles et les usages que doivent respecter les utilisateurs des différents services fournis par la TGIR Huma-Num dans le cadre de sa « grille de services ». Ce texte n’est pas exhaustif en termes de responsabilités, de lois ou de déontologie ; il ne se substitue notamment pas aux lois[^1], ni aux règles et usages en vigueur dans l’enseignement supérieur et la recherche ou dans l’établissement dont relève l’utilisateur[^2].

## Préalable

Dans ce document on désigne par « grille de services », l’infrastructure informatique mise en place par la TGIR Huma-Num et proposant un ensemble de services informatiques connectés entre eux. On désigne de façon générale sous le terme « services informatiques », la mise à disposition de services dédiés au traitement, à la valorisation et à la conservation des données de la recherche en SHS : calcul, espaces de stockage ou d’hébergement, applications proposées et gérées par Huma-Num, serveurs virtuels, archivage à long terme, etc., sans que cette liste soit exhaustive.

## Conditions d’accès

Pour solliciter l’utilisation des services de la Grille de services, l’utilisateur doit relever du monde académique et le projet scientifique doit être validé par la TGIR Huma-Num. Par ailleurs, les projets ou réalisations susceptibles d’utiliser la grille de services doivent s’inscrire dans une démarche privilégiant au moins deux des trois critères suivants :

-   **des besoins spécifiques sur le traitement des données** (enrichissement, annotation, calcul, signalement, etc.)
-   **un engagement sur l’interopérabilité des données** de la recherche et des métadonnées associées (signalement des métadonnées dans le cadre de la plateforme ISIDORE)
-   **une démarche d’archivage à long terme des données**.

Les services de la Grille de sevices sont destinés à être utilisés dans le seul cadre de l’activité professionnelle de l’utilisateur. La fourniture du service est strictement liée au projet pour lequel elle a été ouverte.

La demande d’ouverture d’un ou plusieurs services est à effectuer [par mail à Cogrid](mailto:cogrid@huma-num.fr). Pour permettre à l’équipe d’Huma-Num d’examiner votre demande, le responsable du projet est invité à nous donner le maximum d’informations concernant ses besoins : une courte présentation du projet scientifique, les responsables (scientifiques et techniques), la structure administrative support, les services sollicités, la volumétrie envisagée, le type de données concernées et les technologies mises en œuvre.

Les services suivants peuvent se demander sans passer par Cogrid : ISIDORE, ShareDocs, GitLab, Stylo, Mattermost. Pour ces services, l’utilisateur est invité à se rendre sur [humanid.huma-num.fr](https://humanid.huma-num.fr), créer son compte et demander l’ouverture du service souhaité.

## Responsabilité de l’utilisateur

Les responsables scientifiques et techniques du projet sont garants de la conformité juridique de leurs données. Ils veillent notamment à ce que la diffusion de contenus respecte les lois et règlements. Ils engagent également leurs responsabilités quant à la possibilité de les communiquer. Si les données sont soumises aux dispositions de la loi informatique et libertés, les responsables du projet doivent accomplir les formalités requises par la CNIL et veiller à un traitement des données conforme aux dispositions légales.

Les responsables scientifiques et techniques du projet s’engagent à être en conformité avec le  règlement général de l’union européenne sur la protection des données (RGPD). Le [CIL du CNRS](http://www.cil.cnrs.fr/CIL/spip.php?article3009) propose un certains nombres de ressources pour cela.

Les responsables scientifiques et techniques doivent s’assurer de la pérennité du nommage internet permettant l’accès à leurs ressources.

L’utilisateur s’engage obligatoirement à mettre à jour régulièrement les logiciels et applicatifs qu’il a apportés sur l’infrastructure. À ce titre, il doit s’assurer avant de les implémenter qu’il en a la maîtrise technique lui permettant d’effectuer les mises à jour dans la durée. Huma-Num se réserve le droit de suspendre la fourniture du service si les mises à jour ne sont pas effectuées.

L’utilisateur est soumis aux contraintes des plans d’exploitation de la TGIR Huma-Num et accepte les interruptions temporaires des services, liés notamment aux arrêts pour maintenance. En cas d’interruption de services, l’utilisateur sera informé.

## Responsabilité de la TGIR Huma-Num

La TGIR Huma-Num s’engage à assurer une disponibilité maximale des différents services informatiques proposés à l’utilisateur. Toutefois, elle ne peut être tenue pour responsable de leur indisponibilité.

En cas d’interruption de service, la TGIR Huma-Num mettra tout en œuvre pour rétablir le plus rapidement possible la fourniture du service sans toutefois que ce délai puisse être garanti d’aucune façon. Elle ne sera en aucun cas responsable des préjudices que pourrait causer une interruption de service qu’elle soit de son fait ou due à une cause extérieure.

La TGIR Huma-Num se réserve le droit de prendre, sans préavis, toute mesure destinée à garantir la pérennité et la sécurité de ses services informatiques.

La TGIR Huma-Num n’opère aucune opération ou traitement sur les données en dehors de leur conservation à l’identique. Il en est de même pour les applications apportées par l’utilisateur.

La TGIR Huma-Num met à disposition une adresse pour l’assistance des services : [assistance@huma-num.fr](mailto:assistance@huma-num.fr).

## Durée du service

Les conditions d’utilisation des services de la TGIR Huma-Num prennent effet dès l’ouverture d’un compte sur la grille de services. Ces services peuvent être arrêtés sur demande expresse du responsable de projet ou de la personne mandatée par celui-ci pour prendre sa succession.

La fourniture d’un service prend fin lors de la cessation, même provisoire, de l’activité professionnelle qui l’a justifiée. Il est de la responsabilité de l’utilisateur d’informer la TGIR Huma-Num de la cessation de son rattachement au projet, ou de la fin du projet.

La TGIR Huma-Num se réserve le droit de contacter le responsable du projet en cas :

-   d’inactivité prolongée du service mis en place
-   du non respect manifeste des conditions générales d’utilisation
-   du non respect des critères ayant permis l’accès au service
-   et en cas d’absence de réponse, au-delà de 30 jours, de suspendre le service.

## Obligation de publicité

Huma-Num se réserve le droit de publier la liste des projets, y compris l’information récoltée via le formulaire lors de l’ouverture du service (présentation du projet, type de données traitées, etc.) pour la communication autour de ses différentes activités. Les responsables scientifiques et techniques du projet devront veiller à transmettre régulièrement toutes modifications nécessaires à la TGIR.

L’utilisateur s’engage à citer dans tous les documents relatifs au projet, et sur les supports Web, le soutien apporté par la TGIR Huma-Num.

Cette indication sera suivie du logo d’Huma-Num disponible à cette adresse [www.huma-num.fr/supports-communication/](https://www.huma-num.fr/supports-communication/) et fera un lien vers le site de la TGIR Huma-Num.

Afin d’accorder le meilleur suivi à chacun des projets, toute interaction avec la TGIR devra, autant que possible s’effectuer par voie électronique via les adresses suivantes :

-   [cogrid@huma-num.fr](mailto:cogrid@huma-num.fr) pour les demandes d’accès et pour la mise à jour des données du projet, et
-   [assistance@huma-num.fr](mailto:assistance@huma-num.fr) pour toute demande de support et d’assistance.

## Application de l’accord

L’utilisation du ou des services proposés par Huma-Num équivaut à l’acceptation sans réserve de ces conditions générales d’utilisation. En cas de non-respect des règles énoncées dans cette convention, le(s) service(s) proposé(s) par la TGIR Huma-Num pourra(ont) être partiellement ou totalement suspendu(s).

------------------------------------------------------------------------

[^1]: Il est notamment rappelé que l’ensemble des utilisateurs de la grille de services, quel que soit leur statut est soumis à la législation française en vigueur et notamment :
- la loi du 29 juillet 1881 modifiée sur la liberté de la presse, la loi n°78-17 du 6 janvier 1978 modifiée relative à l’informatique, aux fichiers et aux libertés,
- la législation relative aux atteintes aux systèmes de traitement automatisé de données (art. L323-1 et suivants du code pénal)
- la loi n° 94-665 du 4 août 1994 modifiée relative à l’emploi de la langue française- la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique
- les dispositions du code de propriété intellectuelle relative à la propriété littéraire et artistique

[^2]: À ce titre, l’utilisateur est notamment renvoyé à la lecture de la charte informatique de [RENATER](https://www.renater.fr/chartes), du [CNRS](https://www.cil.cnrs.fr/CIL/spip.php?article1386), du [CC-In2p3](http://cctools.in2p3.fr/baseConnaissance/upload/574/frcharteinformatiquecnrs012007.pdf) et du [CINES](https://www.cines.fr/charte-du-cines-2/).
