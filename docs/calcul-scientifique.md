# Calcul Scientifique et démonstrateur Callisto

!!! note
	- Huma-Num met à disposition un service de calcul scientifique à distinguer du projet de **[démonstrateur Callisto](#demonstrateur-callisto)** instruit par le [HN Lab](https://www.huma-num.fr/hnlab). Ces deux services répondent à des usages différents et leurs modalités d'accès sont également différenciées.

## Le service de calcul scientifique

La TGIR Huma-Num, en partenariat avec le [Centre de calcul de l'IN2P3](https://cc.in2p3.fr) (CC-IN2P3), met à disposition des laboratoires de recherche en SHS un accès à des ressources de calcul sous la forme de deux services distincts, dont le principe est de graduer les demandes par puissance.

- Niveau 1 : la mise à disposition d'un serveur pour du calcul interactif mutualisé entre plusieurs utilisateurs.
- Niveau 2 : l'accès à la ferme de calcul du CC-IN2P3 par soumission de [jobs](https://doc.cc.in2p3.fr/fr/Computing/computing-introduction.html) pour des traitements nécessitants une puissance plus importante.

La puissance de calcul correspond, au nombre de CPU (et de coeur), à la taille mémoire, à l'utilisation de carte accélératrice dédiée (GPU, FGPGA, ...), au volume de données et au temps de traitement.

La graduation permet de distinguer deux niveaux : le niveau 1 pour un calcul nécessitant une puissance inférieure à la capacité d'une machine actuelle, le niveau 2 pour du calcul supérieur ou égal à la capacité d'un serveur. Ceci différencie les deux services et les ressources dédiées.

Dans les deux cas de figure, l'affectation personnelle de serveur ainsi que la réservation d'heures à un projet ne sont pas possibles.
Pour ces types de besoins ainsi que pour des puissances importantes, Huma-Num rédigera vers l'[IDRIS](http://www.idris.fr/jean-zay/cpu/jean-zay-cpu-hw.html) qui dispose des infrastructures les plus complètes au niveau national sur ce type de traitements.

Enfin la puissance de calcul étant limitée, la TGIR HUMA-NUM ainsi que le CC-IN2P3 se réservent le droit de ne pas ouvrir le service ou de désactiver un compte si celui-ci dépasse les ressources réellement disponibles.

**Usage des ressources**

L’usage des ressources informatiques de la TGIR HUMA-NUM et du CC-IN2P3 sont soumises à l’acceptation des règles définies dans la charte informatique du CNRS et la charte déontologique RENATER disponibles ci-dessous :

- [charte informatique du CNRS](https://atrium.in2p3.fr/f94b7fcd-1d4f-4b63-85e5-5b102ba80333)
- [charte déontologique RENATER](https://www.renater.fr/telechargement%2C1392)

**Formule de remerciements**

Pour toute publication scientifique qui a bénéficié des services de la TGIR HUMA-NUM et ou du CC-IN2P3 pour le traitement des données, nous suggérons d’insérer le texte de remerciements suivant :
> (FR) Nous remercions le CNRS/TGIR HUMA-NUM et le Centre de Calcul IN2P3 (Lyon - France) pour la fourniture des ressources informatiques et de traitement des données nécessaires à ce travail.
> (EN) We gratefully acknowledge support from the CNRS/TGIR HUMA-NUM and IN2P3 Computing Center (Lyon - France) for providing computing and data-processing resources needed for this work.

### Niveau 1 : serveur de calcul interactif mutualisé

Ce service est orienté pour les besoins spécifiques de traitement statistique sous R, nécessitant classiquement
d'exécuter des modèles sur quelques heures.

L'accès se fait via trois modes :

- Web via l'IDE R Rstudio (https://r-tools.huma-num.fr)

- Web via l'IDE Jupyter (https://jha-tools.huma-num.fr)

- En ligne de commande via SSH ou des IDE a exécution distante comme vsCode

Quelle que soit la méthode d'accès, l'authentification étant un compte local,
l'espace de fichiers est identique pour l'utilisateur, il est donc permanent
et sauvegardé une fois par jour.


#### Langage et environnement disponible

Il est compilé et/ou mise à jour environ tous les 3 mois les langages ci-dessous :

- R version 3.6, 4.0, 4.1 avec environ 600 packages dont les principales bibliothèques géographiques
- Julia 1.6 sans packages
- Python 3.8, 3.9 sans packages à part ceux de base.

L'utilisateur peut installer localement dans son compte d'autres packages (cas de R ou Julia), environnement
(cas de python), ou kernel (cas de jupyter)


#### Demande de création d’un compte

L’accès au service nécessite de faire une demande d’accès via l'adresse du comité de la grille d'Huma-Num pour que la demande soit étudiée : [cogrid@huma-num.fr](cogrid@huma-num.fr) en précisant

- Nom, Prénom :
- Email :
- Statut, laboratoire et établissement de rattachement :
- Cadre d’utilisation (projet de recherche, thèse ...) :
- Titre du projet :
- Type de programme (ANR, ERC,... ) :
- Courte description :

Pour les étudiants en thèse :

- Sujet :
- Directeur de thèse :
- Date estimée de soutenance :

Le service est uniquement réservé au seul membre des laboratoires de recherche du CNRS et des universités publiques pour des problématiques de calcul en sciences humaines et sociales. Les demandes pour des formations et/ou des cours ne sont pas possible (les universitsés disposant normalement de leur propre plateforme)

#### Détails techniques

Le service est opéré par un serveur **non** virtualisé ayant les caractéristiques suivantes :

- OS : CentOS 8 (arch x86_64)
- CPU: 2 * AMD EPYC 7542 32-Core Processor 2.9 MHz = 64 cores
- RAM: 16 * 64 GB 3200 MHz DDR4 = 1024 GB
- GPU: 2 * Nvidia A100 40G
- Disque : 28To SSD interne

Les calculs sont limités à l'utilisation 19 cores et 96Go de Ram.
Les GPU Nvidia A100 sont partitionnés via MIG, par conséquenc il n'est pas possible d'utiliser les bibliothèques graphique (Vulkan, DirectX, OpenGL).
Chaque compte ne peut accéder qu'à une fraction de la GPU.

#### Durée du service et utilisation du service

Quelques informations sont importantes à noter dans l'utilisation du service :

- Le serveur n’a pas vocation à faire de la conservation de données. **Il est donc demandé expressément aux utilisateurs de ne pas stocker les données une fois les calculs réalisés**.
- La service a été planifié pour des comptes à 90Go en moyenne.  
- Étant un serveur partagé entre plusieurs comptes, **il est interdit de modifier le système d’exploitation, ou les packages du serveur**.
- Le service est prévu pour être maintenu jusqu'à fin 2023, la TGIR ne s'engage pas au dela
de cette période pour un renouvellement sur plus de puissance ou un service similaire.


### Niveau 2 : usage de la ferme de calcul de l'IN2P3

#### Demande de création d’un compte

L’accès au service nécessite de faire une demande d’accès via l'adresse du comité de la grille d'Huma-Num pour que la demande soit étudiée : [cogrid@huma-num.fr](cogrid@huma-num.fr) avec les mêmes informations pour le premier niveau.

Le nombre de comptes pouvant annuellement utiliser la ferme de calcul est limité, car planifié en nombre d'heures d'utilisation d'une année sur l'autre avec le CC-IN2P3. Huma-Num procède à une estimation d'un nombre d'heures de calcul pour les SHS, actuellement 1200H en GPU par mois.

Huma-Num ne peut ainsi garantir, au regard des conditions évoquées ci-dessus, un accès systématique à ce service.

#### Caractéristiques techniques et documentation

L'ensemble des caractéristiques techniques de la ferme de calcul est disponible ici :

- [http://cctools.in2p3.fr/mrtguser/info\_sge\_parc.php](http://cctools.in2p3.fr/mrtguser/info_sge_parc.php).

La documentation pour l’utilisation du cluster de calcul

- [https://doc.cc.in2p3.fr/fr/Computing/computing-introduction.html](https://doc.cc.in2p3.fr/fr/Computing/computing-introduction.html)

La partie spécifique pour le cluster GPU

- [https://documentation.pages.in2p3.fr/collab/gpu/CC_GPU_Farm/index.html](https://documentation.pages.in2p3.fr/collab/gpu/CC_GPU_Farm/index.html)

Utilisation des conteneurs

- [https://doc.cc.in2p3.fr/fr/Software/software/singularity.html](https://doc.cc.in2p3.fr/fr/Software/software/singularity.html)

Utilisation de jupyter

- [https://doc.cc.in2p3.fr/fr/Computing/jn-platform.html](https://doc.cc.in2p3.fr/fr/Computing/jn-platform.html)

#### Utilisation du service

L'utilisation du service nécessite la bonne prise en main de la documentation mise en exergue dans la section précédente (utilisation d'un séquenceur de jobs, estimation de la durée d'un script, etc.).

Une fois les traitements réalisés, les fichiers doivent être supprimés du système de stockage, qui n’a pas vocation à conserver des données.

### Autre infrastructure permettant l'accès à une ferme de calcul

Pour des besoins plus importants il est possible de faire appel directement à l'[IDRIS](http://www.idris.fr/) qui a reçu des financements nationaux pour la mise en oeuvre de 2 696 cartes GPU. Le détail de cette configuration est décrite ici :
- [http://www.idris.fr/jean-zay/cpu/jean-zay-cpu-hw.html](http://www.idris.fr/jean-zay/cpu/jean-zay-cpu-hw.html).


## Démonstrateur Callisto

Depuis fin 2020, parallèlement à ce service Calcul Scientifique, la TGIR Huma-Num expérimente la mise en place d'une *preuve de concept* sous la forme d'une plateforme  proposant une instance JupyterHub et JupyterLab spécifiquement dédiées aux pratiques de traitements des données et des informations de recherche en SHS. Initiée et coordonnée par le HN Lab, **[Callisto](https://hnlab.huma-num.fr/blog/2021/05/26/callisto-un-demonstrateur-jupyter/)**, a pour but de tester et de concevoir l'intégration de services d'une plateforme de *data science* en lien avec l'écosystème de la TGIR et le besoin des communautés SHS. Callisto offre un fonctionnement en « hub » autour de :

- JupyterLab
- La mise à dispotition de plusieurs *kernels* dédiés a des besoins SHS : R, Python pour le *Deep Learning*, etc.
- La mise à disposition de carnets Python pour la prise en mains des outils de science des données (*Deep Learning* et *Machine Learning*, et à terme des jeux de données pour l'entrainement de réseaux de neurones)
- JupyterHub, couplé à HumanID (le dispositif de la TGIR Huma-Num pour [l’authentification des utilisateurs](https://callisto.huma-num.fr))
- Gitlab ([forge de dépôt déjà disponible au sein de la TGIR Huma-Num](https://documentation.huma-num.fr/gitlab/) permettant le versionnage de documents, codes sources, scripts, etc)

Conçu spécifiquement pour les usages en SHS, le démonstrateur Callisto a ainsi vocation à intégrer des éléments pertinents sur le plan scientifique pour la communauté SHS et est animée par [un groupe de travail ouvert au sein du HN Lab](https://hnlab.huma-num.fr/blog/2021/03/23/lancement-du-groupe-de-travail-callisto/). La faisabilité de Callisto étant encore à l'étude, ce service n'est actuellement pas disponible dans la grille de services de la TGIR Huma-Num.
