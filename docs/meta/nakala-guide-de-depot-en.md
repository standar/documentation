---
lang: fr
---

!!! Note  
    Document in progress


# NAKALA Repository Guide

Metadata quality is emphasized in the FAIR principles
principles as a means to achieve the intended goals (making data easy to find, accessible, interoperable and 
accessible, interoperable and reusable).

NAKALA's metadata is essentially expressed in Dublin-Core vocabulary.
In the following, we provide some tips for coding this metadata.

## The Dublin-Core model

The "Dublin-Core simple" model proposes 15 very generic description headings

- contributor, coverage,
creator, date, description, format, identifier, language, publisher, relation, rights,
source, subject, title, type 

The "qualified Dublin Core" includes 

- additional headings (audience, provenance, rightsholder...)
- refinement qualifiers allowing to specify the basic headings 
(for example: available, created, dateAccepted, dateCopyrighted, dateSubmitted, issued, modified, valid
are all qualifiers that specify the generic notion of date). 
- encoding schemes and controlled vocabularies to express the values of a field
 (for example: DCMIType, W3CDTF...).

### Description principles
A description must be rich, precise and accurate.

- Prefer the use of qualified DC terms when possible rather than simple DC terms;
- When the content of a heading is expressed in a language, specify it using the lang attribute;
- Wherever relevant, use formal syntax or controlled vocabulary rather than free text
 rather than using free text;
 - When several pieces of information of the same nature must be specified, use the same term several times. 
 Do not use a system based on separator characters.





----------

### NAKALA vocabulary elements (mandatory)

#### Title (nakala:title)
Title or name given to the resource. 
Specify the language of the title.

#### Type (nakala:type)
The primary type of the resource. This type must be expressed in the closed vocabulary proposed by Nakala.
If you want to complete the resource by adding other types or by specifying the type
or genre, use dcterms:type 

#### Authors (nakala:creator)
Authors of the resource.



#### Date (nakala:created)
Date of creation of the content of the resource, not the date of creation of its digitized form
in case of post digitization.

If the deposited resource represents only a digital avatar of the object you describe, 
for example the digitization of an old manuscript, indicate the date of creation of the latter. 

Use W3CDTF syntax (YYYY, YYYY-MM or YYYY-MM-DD) or the value "Unknown."


#### License (nakala:license) 
License to use the resource. 
Choose a license present in the Nakala repository which currently contains more than 400 values.


----------

### Items automatically calculated by NAKALA

- The names of the deposited files 
- The sizes of the deposited files 
- The formats of the uploaded files 
- The fingerprints of the uploaded files 
- The date of the submission 
- The submitter's identifier 
- the identifier (DOI) of the deposit 




----------

### Additional metadata (Dublin-Core vocabulary)


#### dcterms:type

The nature or kind of the resource. Complementary to nakala:type

#### dcterms:available
The date the resource is or will become available.

#### dcterms:abstract
Summary of the content of the resource in free text format. 
Specify the language of the abstract.

#### dcterms:accessRights
Information about the conditions of access to the resource or its security status.
For example: "Freely available" or "Embargoed due to..."
Specify the language of the field content.

#### dcterms:alternative
Secondary title, short title or other name given to the resource.
Specify the language of the field content. 

#### dcterms:audience
Type of population for which the resource is intended or useful.
Specify the language of the field content.

#### dcterms:bibliographicCitation
A bibliographic citation for the resource.
Specify the language of the field content.

#### dcterms:conformsTo
The standard, norm, or formal specification to which the described resource conforms.
If possible, indicate its identifier (DOI, ISBN...), a form of citation, its name or a description.
If necessary, indicate the language of the field content.

#### dcterms:contributor
Entity that contributed to the content of the resource (individual, institution, organization, service),
other than the entity identified in nakala:creator or by specifying the nature of its contribution.


#### dcterms:coverage
Scope, coverage of the resource.
If it is a geographic scope or coverage use dcterms:spatial instead. 
If it is a temporal scope or coverage use dcterms:temporal instead. 
Specify the language of the field content.


#### dcterms:date
Date in the life cycle of the resource. When possible, use more precise terms (available, created, date, etc.)
(available, created, dateAccepted, dateCopyrighted, dateSubmitted, issued, modified, valid)

When possible, use the W3CDTF syntax by specifying the dcterms:W3CDTF encoding. Cf. 
[https://www.w3.org/TR/NOTE-datetime](https://www.w3.org/TR/NOTE-datetime) for the different acceptable formats:
year, year+month, year+month+day, year+month+day+hour+minutes+seconds (eg "1997-07-16T19:20:30+01:00")

It is also possible to define a period by specifying the type dcterms:Period. Cf 
https://www.dublincore.org/specifications/dublin-core/dcmi-period/](https://www.dublincore.org/specifications/dublin-core/dcmi-period/)
for the different acceptable formats (eg "name=The Great Depression; start=1929; end=1939;")

#### dcterms:description
Free text description of the content of the resource. 
Specify the language of the description.
If the description is a summary of the content use dcterms:abstract instead. 
If the description is a table of contents use dcterms:tableOfContents instead.

#### dcterms:educationLevel
Education level of the audience for which the resource is intended.
Specify the language of the description.

#### dcterms:extent
The size or duration of the resource.

For durations, use when possible an xsd:duration encoding expressed as a string along the lines of Pn
character string on the model of PnYnMnDTnHnMnS (eg for a duration of 2 hours 15 minutes PT2H15S). Cf. the 
XML Schema Part 2: Datatypes" specifications on the W3C site
website[https://www.w3.org/TR/] for more details.

For the sizes indicate the unit (eg "120 pages"). Do not indicate the size in bytes of the file which is already expressed
in the nakala:? property.

#### dcterms:format
The file format, physical medium or dimensions of the resource.

Eg "in-quarto to 2 col"

#### dcterms:identifier
Identifier, call number of the described resource. 
Do not put the DOI identifier which is already expressed in the property nakala:?
#### dcterms:instructionalMethod
A process used to generate knowledge, attitudes and skills,
for the support of which the described resource is intended.
#### dcterms:language
The language of the resource.
#### dcterms:mediator
An entity that mediates access to the resource and for whom the resource is intended or useful.
#### dcterms:medium
The material or physical medium of the resource.
#### dcterms:provenance
A statement of all changes in ownership and custody of the resource since its creation that are important to its authenticity, integrity and interpretation.
#### dcterms:publisher
An entity responsible for making the resource available.
#### dcterms:relation
A related resource.
When possible, use more specific terms (conformsTo, hasFormat, hasPart, hasVersion, 
isFormatOf, isPartOf, isReferencedBy, isReplacedBy, isRequiredBy, isVersionOf, references, replaces, requires
#### dcterms:rights
Information about the rights held in and on the resource.
#### dcterms:rightsHolder
A person or organization that owns or operates the rights to the resource.


#### dcterms:rightsHolder
A person or organization that owns or operates the rights to the resource.
#### dcterms:source
A related resource from which the described resource is derived.
#### dcterms:spatial
The spatial coverage or scope of the resource.
#### dcterms:subject
The subject of the resource.
#### dcterms:tableOfContents
The table of contents of the resource in free text format. 
Specify the language of the table of contents.


#### dcterms:temporal
Temporal coverage of the resource

### Expert metadata (Dublin-Core vocabulary)

#### Collection-specific metadata
To be used only for collection metadata.

| property | definition |
|--|--|
| The method by which resources can be added to a collection. Use the "Collection Description Accrual Method Vocabulary" <https://www.dublincore.org/specifications/dublin-core/collection-description/accrual-method/> if possible.
| The frequency with which resources are added to a collection. Use the "Collection Description Frequency Vocabulary" <https://www.dublincore.org/specifications/dublin-core/collection-description/frequency/> if possible.
| The policy that governs the addition of resources to a collection. Use the "Collection Description Accrual Policy Vocabulary" <https://www.dublincore.org/specifications/dublin-core/collection-description/accrual-policy/> if possible.

#### Specialized date metadata

When possible, use W3CDTF syntax by specifying it in the encoding. Cf. 
[https://www.w3.org/TR/NOTE-datetime](https://www.w3.org/TR/NOTE-datetime) for the different acceptable formats:
year, year+month, year+month+day, year+month+day+hour+minutes+seconds (eg "1997-07-16T19:20:30+01:00")

It is also possible to define a period by specifying the type dcterms:Period. Cf 
https://www.dublincore.org/specifications/dublin-core/dcmi-period/](https://www.dublincore.org/specifications/dublin-core/dcmi-period/)
for the different acceptable formats (eg "name=The Great Depression; start=1929; end=1939;")

| date | definition |
|--|--|
| date:dateAccepted** | date the resource was accepted.
| The date the resource was accepted. **dcterms:dateCopyrighted** | Date copyright was filed.
| Date the resource was submitted. **dcterms:dateSubmitted** | Date the resource was issued.
| Date the resource was formally issued (e.g., published).
| Date the resource was modified
| The date (often a range) that a resource is valid.

#### Specialized relationship metadata

| relationship | definition |
|--|--|
| A related resource that is superseded, moved, or replaced by the resource being described.
| A related resource that supplants, displaces, or replaces the described resource.
| A linked resource that is required by the described resource to support its function, distribution, or consistency.
| A linked resource that requires the described resource to support its function, distribution, or consistency.
| A linked resource that is a version, edition, or adaptation of the described resource.
| A linked resource that is a version, edition, or adaptation of the resource being described.
| A linked resource that is essentially the same as the pre-existing described resource but in a different format.
| A linked resource that is substantially the same as the described resource but in a different format.
| A linked resource that is physically or logically included in the described resource.
| A linked resource in which the described resource is physically or logically included.
| A related resource that is referenced, cited, or pointed to by the described resource.
