---
lang: en
---

!!! Note  
    Document in progress


# Evolution(s) of NAKALA contents 

In case of evolution, the data producers will be informed directly via the mailing list. If an action or a decision from them is necessary, Huma-Num will provide them with all the necessary documentation and support.

Users will be informed via the usual Huma-Num distribution channels (i.e. Blog, Mailing lists etc.).

## Metadata evolution 

The evolution of NAKALA's metadata may be necessary for different reasons:
- Evolution of the repositories used: when the content of the repositories used is updated, this may cause changes in the content of the metadata, for example if terms are modified or disappear. New repositories will be sought regularly to assess the relevance of their integration into NAKALA. The integration of these new repositories may allow an enrichment of the existing contents and possibly require an alignment with the terms used previously;   
- Evolution of the metadata model: The current model, the [DublinCore](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/), for metadata is a generic model. It is planned to gradually add other metadata models according to their relevance and the demands of different user communities. In the same way as for the evolution of the repositories, these evolutions could allow an enrichment of the existing contents.  
- Revision to be carried out in case of technological evolution of the NAKALA platform: The cycles of technological evolutions are getting shorter and shorter and impose frequent updates to avoid technological obsolescence. In some cases, these changes may lead to changes in the metadata, which will be carried out as far as possible in an automated manner. 
- Resumption to be carried out in the event of regulatory evolution: The content of the metadata is also likely to evolve because of a change of regulation or general policy. One can quote for example, the implementation of the RGPD regulation or the obligation to specify a license. In general, the type of metadata modifications caused by these evolutions cannot be treated directly because it may require a decision: Huma-Num will contact the producers to inform them and assist them in these evolutions. 

## Data evolution 

During the regular controls carried out by Huma-Num (Cf. [Frequently Asked Questions](https://documentation.huma-num.fr/meta/nakala-faq-en/)), the non conformity of the data format with the specifications can be detected. The producer will be informed and it will be suggested to him to make a correction with adapted tools if they are available. 

The evolution of the data itself, in particular its format, may be necessary for several reasons:
- The format has become obsolete, for example the tools to generate and verify it are no longer maintained;  
- The format is no longer free, for example its use requires the payment of a license;   
- The format is no longer used by any community;  
- The format is not optimal for use with tools associated with NAKALA (e.g. IIIF server);  
etc.

