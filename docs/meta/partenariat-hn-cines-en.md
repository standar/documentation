---
lang: en
---

!!! Note  
    Document in progress


# Partnership between Huma-Num and the CINES

The TGIR Huma-Num offers the community of producers of digital data in Humanities and Social Sciences a preservation service on the long term. It relies, for this activity, on the infrastructure and skills of a labeled center, the Centre Informatique National de l'Enseignement Supérieur (CINES).  
The relationship between Huma-Num and the CINES is governed by an agreement for a period of 4 years.

As part of its partnership with CINES, Huma-Num provides the link with the SHS communities and supports them in their preservation project. Huma-Num provides funding, identifies new data formats to preserve, proposes them to CINES and participates in the process of integration of these formats on the CINES platform. 

For example, a study was conducted by Huma-Num in collaboration with experts from the TEI community to introduce this format at CINES. The level of requirement defined for the integration of this format at CINES has allowed in return to all producers using this format to improve the quality of their production in particular their structuring and their documentation. 

CINES provides technical expertise on formats, performs archival monitoring and takes responsibility for the long-term preservation of data entrusted to it on behalf of Huma-Num, anticipating the risks associated with technological obsolescence by quality assurance procedures and preservation planning, and replicating data on a remote site to prevent any type of disaster.  
To learn more about the archiving service offered by CINES:  
[https://www.cines.fr/archivage/](https://www.cines.fr/archivage/)



Early 2021, 8 archiving projects (excluding NAKALA) are underway:
 

![CINES archiving projects](../media/meta/archivage_cines.png)

