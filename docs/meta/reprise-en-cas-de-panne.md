---
lang: fr
---

!!! Note  
    Document en cours de rédaction


# Plan de reprise en cas de panne

Ce document concerne les composants qui sont nécessaires au fonctionnement de l'entrepôt NAKALA

## Panne(s) matérielle(s) 

Huma-Num effectue la jouvance de son infrastructure de manière régulière. 
Ainsi, tous les matériels de Huma-Num sont couverts par la garantie apportée par les constructeurs : en cas de panne, une intervention sera réalisée à J+1 pour les jours ouvrés.  

De même les matériels gérés par le centre de calcul [CCIN2P3](https://cc.in2p3.fr/) qui héberge l'infrastructure de Huma-Num sont couverts par des garanties équivalentes.   

On peut noter que dans l'ensemble, avec un recul d'une dizaine d'années, les pannes matérielles sont très rares aujourd'hui. 
Cependant, une panne est toujours possible : même si Huma-Num ne peut garantir formellement un fonctionnement totalement permanent, toutes les précautions sont prises pour minimiser les interruptions de service.  


### En cas de panne du [pare-feu](https://fr.wikipedia.org/wiki/Pare-feu_(informatique)) 

Huma-Num s'est doté d'un pare-feu qui permet d'affiner les filtrages effectués pour ses besoins spécifiques. Il est utilisé en complément du pare-feu général du centre de calcul qui héberge l'infrastructure de Huma-Num.

Le pare-utilisé par Huma-Num n'est pas redondé pour le moment, il est prévu de l'envisager dans le cadre du projet COMMONS (financé par le PIA3) qui débutera fin 2021. 
En cas de panne, en attendant l'intervention du constructeur, il est possible de continuer à fonctionner de manière légèrement  dégradée en terme de filtrage en n'utilisant que la protection du pare-feu du centre de calcul. 


### En cas de panne d'un composant réseau 

Les composants réseaux sont gérés par le centre de calcul qui héberge l'infrastructure de Huma-Num. 

Le pare feu du centre de calcul n'est pas redondé mais est couvert par un contrat spécifique qui garantit la disponibilité permanente des pièces détachées. 

La connexion au réseau du centre de calcul est assurée par le groupement [RENATER](https://www.renater.fr/en/history) dont l'une des missions est de  founir une connexion fiable et permanente la communauté enseignement recherche. 

### En cas de panne d'une machine physique 

Les services nécessaires à NAKALA sont redondés sur des machines physiques différentes. En cas de panne d'une machine, l'autre instance du service prend le relais. C'est le cas pour l'application NAKALA, le serveur de base de données et le Triple Store. 

Par ailleurs, ces services sont virtualisés (i.e. hébergés sur des machines virtuelles) : des sauvegardes régulières (snapshots) de ces machines sont faites ce qui permet de les restaurer sur une autre machine en cas de panne. Ce processus de restauration n'est pas immédiat et peut provoquer une indisponibilité du service durant un certain temps. 

### En cas de panne d'un [NAS](https://fr.wikipedia.org/wiki/Serveur_de_stockage_en_r%C3%A9seau)  

Les NAS utilisés par Huma-Num intègrent des dispositifs de sécurisation de haut niveau, notamment :  
-  la partie "contrôleur" de disque est doublée ;    
-  la partie réseau est également redondée ;  
-  le NAS utilise la technologie [RAID](https://fr.wikipedia.org/wiki/RAID_(informatique)) qui lui permet de fonctionner en cas de panne d'un ou plusieurs dispositifs de stockage .  

Dans le cadre du contrat de service associé aux NAS, le constructeur est prévenu en temps réel des éventuels problèmes (matériel ou logiciel) et ses personnels sont autorisés à intervenir dans le centre de calcul.   

En cas d'un sinistre important qui ne serait pas couvert par ces dispositifs de sécurité, il sera toujours possible de restaurer les données à partir des sauvegardes, mais ce processus prendra un certain temps avant que les données soient à nouveau rendues disponibles. 


## Utilisation des sauvegardes 


### En cas de problèmes sur des données (effacement, remplacement etc.)

Plusieurs niveaux de sécurité sont proposés par Huma-Num pour les données stockées sur son infrastructure : 
- pour les données stockées sur les dispositifs de type NAS utilisés par NAKALA, une image (snapshot) est réalisée automatiquement par le NAS à intervalles réguliers (chaque heure) ce qui permet de restaurer très rapidement un fichier modifié ou effacé. Bien entendu, la capacité des disques n'étant pas infinie, ces nombreuses images ne peuvent être conservées indéfinimement et sont remplacées au fur et à mesure. Si une donnée a été effacée ou modifiée depuis un temps qui dépasse ce cycle de création d'images, il est possible de la restaurer à partir des sauvegardes effectuées sur bande magnétique, mais cela nécessite une intervention qui prendra plus de temps. 
- pour les données stockées sur les disques "locaux" des machines virtuelles, elles  sont sauvegardées de manière spécifique.


### En cas de problème avec une base de données 

Les bases de données utilisées par NAKALA sont sauvegardées quotidiennement : une base peut donc être restaurée rapidement en cas de besoin. Le serveur étant par ailleurs redondé, le risque est réduit d'autant.

### Sauvegardes sur bandes magnétiques 

Les données sont sauvegardées de manière complémentaire sur des bandes magnétiques par le robot de sauvegarde du centre de calcul avec une durée de rétention longue.

Une copie des données dans un autre lieu géographique est à l'étude. Des premiers contacts ont été établis avec le [CINES](https://www.cines.fr) et d'autres éventualités sont à l'étude : cette sauvegarde sera mise en oeuvre dans le cadre du projet COMMONS.





