---
lang: fr
---

!!! Note  
    Document en cours de rédaction


# NAKALA : Foire Aux Questions 


* [Données dans NAKALA](#donnees-dans-nakala)
    * [Quelles données peut-on déposer dans NAKALA ?](#quelles-donnees-peut-on-deposer-dans-nakala)
    * [Propriété des données déposées dans NAKALA](#propriete-des-donnees-deposees-dans-nakala)
    * [Taille des données](#taille-des-donnees)
    * [Comment sont gérés les identifiants  de type Digital Object Identifier](#comment-sont-geres-les-identifiants-de-type-digital-object-identifier)
    * [Quels sont les différents statuts des données dans Nakala ?](#quels-sont-les-differents-statuts-des-donnees-dans-nakala)
    * [Quels contrôles sont effectués sur les données ?](#quels-controles-sont-effectues-sur-les-donnees)
        * [Au moment du dépôt](#au-moment-du-depot)
        * [Contrôles réguliers](#controles-reguliers)
        * [Lors de la demande de préservation à long terme](#lors-de-la-demande-de-preservation-a-long-terme)
    * [Sécurité des données](#securite-des-donnees)
        * [Où sont hébergées les données ?](#ou-sont-hebergees-les-donnees)
        * [Comment sont sauvegardées les données ?](#comment-sont-sauvegardees-les-donnees)
        * [Comment sont sauvegardées les métadonnées ?](#comment-sont-sauvegardees-les-metadonnees)    
* [Continuité du service NAKALA](#continuite-du-service-nakala)
* [NAKALA et Huma-Num ](#nakala-et-huma-num)
    * [Quel est le statut de Huma-Num ?](#quel-est-le statut-de-huma-num)
    * [Comment est financée Huma-Num ?](#comment-est-financee-huma-num)
    * [Et si Huma-Num disparait ?](#et-si-huma-num-disparait) 



## Données dans NAKALA


### Quelles données peut-on déposer dans NAKALA ?

Tous types de données peuvent être déposés dans NAKALA à condition qu'il s'agisse de données de la recherche (e.g. NAKALA n'accepte pas de données de type administratif). 

Le type de format n'est pas imposé, mais il est (fortement) recommandé d'utiliser des formats ouverts (Cf. [Préparer ses données](https://documentation.huma-num.fr/nakala-preparer-ses-donnees/)).

Les données doivent être documentées le plus finement possible. Cinq métadonnées sont obligatoires, mais il est (fortement) recommandé d'en utiliser plus (Cf. [Guide de description](https://documentation.huma-num.fr/nakala-guide-de-description)).


### Propriété des données déposées dans NAKALA 

Les données déposées dans NAKALA restent la propriété du déposant et demeurent sous sa responsabilité.

Les données publiées ne peuvent pas être supprimées sauf pour des cas de force majeure,
auquel cas l'opération de suppression est effectuée après vérification par l'équipe NAKALA.
Une trace associée à l'identifiant pérenne est systématiquement conservée (i.e. "pierre tombale"). 
La supression définitive de l'information ne sera effective qu'après un temps de latence
en raison du rafraichissement des supports et de la rotation des sauvegardes. 

La réutilisation des données est régie par la licence qui leur est associée : pour cette raison, la licence est une métadonnée obligatoire.


### Taille des données 

Il n'y a pas formellement de limitation pour la taille des données déposées. Cependant, s'il est envisagé de déposer de gros volumes (e.g. supérieur à 10 GOs par fichier), il est nécessaire de prendre contact avec l'équipe NAKALA au préalable. 


### Comment sont gérés les identifiants de type Digital Object Identifier

Un identifiant de type DOI est affecté à tous les dépôts (e.g. [10.34847/nkl.f11cyqlk](https://doi.org/10.34847/nkl.f11cyqlk)), ce qui permet de citer la donnée de manière normalisée et d'y accéder de manière pérenne (Cf. via l'interface de NAKALA ou directement via le site [CrossCite](https://citation.crosscite.org/)).

Comme cela a été mentionné plus haut, lorsqu'une donnée est supprimée, une mise à jour des métadonnées est effectuée pour conserver une trace du dépôt dans NAKALA. 

### Quels sont les différents statuts des données dans NAKALA ?

Une donnée déposée dans NAKALA peut avoir un statut différent en fonction de sa progression dans le cycle de vie :  
- Donnée déposée : donnée qui est en cours de documentation avant d'être publiée et non accessible   
- Donnée publiée (avec ou sans embargo) : donnée documentée qui est publiée et accessible si elle n'est pas sous embargo  
- Donnée supprimée : donnée qui a été publiée dans NAKALA et dont l'utilisateur a demandé la suppression à l'équipe NAKALA. Une trace de sa présence est conservée dans les métadonnées associées à l'identifiant DOI 
- Donnée préservée au CINES : donnée publiée dans NAKALA qui a été déposée au CINES après un audit (e.g. vérification du format) et une préparation (e.g. organisation et documentation) préalable au dépôt au CINES  

### Quels contrôles sont effectués sur les données ?

#### Au moment du dépôt :
Différents contrôles sont effectués au moment du dépôt de la donnée pour la validation, en voici quelques exemples :  
-  Vérification de la présence des métadonnées obligatoires ;  
-  Les valeurs de "nakala:license"  doivent être issues du référentiel des licences de NAKALA ;   
-  Les valeurs de "nakala:type" doivent être issues du référentiel des types de NAKALA ;  
-  Le code ISO de la langue d'une métadonnée doit appartenir au référentiel des langues de NAKALA (Norme ISO-639-1 quand cela est possible, sinon Norme ISO-639-3);    
-   La valeur de la date "nakala:created" peut être vide ou doit être une chaine de caractères qui respecte le format "AAAA", "AAAA-MM", "AAAA-MM-JJ" ;  
etc.  

#### Contrôles réguliers :

##### Automatisés 
Dans le cadre du projet [HNSO](https://www.huma-num.fr/projets/), Huma-Num étudie la possibilité de mettre en place un indice de qualité calculé sur un ensemble de critères contrôlables de type :

Vérification des types de formats utilisés pour les fichiers et de la conformité des fichiers à ces formats (Cf. [Préparer ses données](/nakala-preparer-ses-donnees/)) ;  
Vérification du nombre et de la qualité des métadonnées (e.g. par rapport à des référentiels) ;
etc.

Une comparaison des empreintes des fichiers est effectuée régulièrement pour vérifier l'intégrité des fichiers de données.

##### Manuels 
Huma-Num développe la mise en production d'audits manuels de certaines collections en se basant sur différents critères de sélection (e.g. demande des producteurs, nouveaux depôts, choix aléatoire, etc.). Ces audits feront l'objet de bilans fournis au producteur qui donneront lieu à un dialogue pour améliorer la qualité des données déposées. 


#### Lors de la demande de préservation à long terme 
Lorsque la demande de préservation à long terme est effectuée, un audit des données à préserver est effectué par le pôle "Données et accompagnement des utilisateurs".

Des échanges ont lieu pour mettre les données (et les métadonnées) en conformité avec les exigences attendues pour la préservation à long terme :  
-   Organisation générale des données ;  
-   Qualité des formats utilisés et conformité des données aux spécifications des formats ;  
-   Vérification des métadonnées et ajout d'informations nécessaires à la préservation à long terme (e.g. statut, communicabilité etc.) ;    
etc.

Une fois ces différents points examinés, le choix du type de préservation à long terme est effectué au sein d'un "comité de liaison" définit par la convention de collaboration avec le CINES, [partenaire de Huma-Num](https://documentation.huma-num.fr/partenariat-hn-cines) pour la préservation. 

## Sécurité des données 

### Où sont hébergées les données ?
Les données déposées dans NAKALA sont stockées sur des serveurs gérés par Huma-Num et hébergés au [centre de calcul de l'IN2P3](https://cc.in2p3.fr/). Ce centre a été créé pour gérer les données produites en physique des particules, physique nucléaire et physique des astroparticules.

Cet important centre national est sécurisé d'un point de vue matériel (e.g. redondance de l'alimentation électrique, des dispositifs réseau, des systèmes de refroidissement).

### Comment sont sauvegardées les données ?

Les données de NAKALA sont stockées sur une dispositif de stockage réseau de type [NAS](https://fr.wikipedia.org/wiki/Serveur_de_stockage_en_r%C3%A9seau). Une image des données (snapshot) est réalisée à intervalles réguliers, ce qui permet de restaurer rapidement les données en cas de problèmes.

Par ailleurs, une sauvegarde sur bande est effectuée quotidiennement sur le robot de sauvegarde du [CCIN2P3](https://cc.in2p3.fr/)  en utilisant le logiciel [TSM](https://fr.wikipedia.org/wiki/IBM_Tivoli_Storage_Manager) édité par IBM. 


### Comment sont sauvegardées les métadonnées ?

Les métadonnées de NAKALA sont stockées dans une base de données SQL (MariaDB) qui est sauvegardée quotidiennement sur l'infrastructure de Huma-Num. 

Les métadonnées sont également exposées en format RDF via un [Triple-Store](https://nakala.fr/sparql) (GraphDB) qui est lui aussi sauvegardé quotidiennement sur l'infrastructure de Huma-Num.

## Continuité du service NAKALA

Le service NAKALA est hébergé sur l'infrastructure de Huma-Num qui dispose d'un plan général de reprise en cas de panne.

Plus spécifiquement, la redondance du service NAKALA est assurée par :  
- l'application NAKALA est redondée sur deux machines différentes en utilisant un outil de répartition ([HAProxy](http://www.haproxy.org)), ce qui permet d'éviter les interruptions de service en cas de panne ;  
- les données sont stockées sur un dispositif de type NAS qui permet de restaurer rapidement une donnée. En complément, des sauvegardes sont effectuées sur des bandes magnétiques quotidiennement ;  
- Les métadonnées sont stockées dans une base de données relationnelle "classique" qui est sauvegardée quotidiennement. Ces métadonnées sont également stockées en format RDF dans un Triple-Store qui est sauvegardé quotidiennement .    
  
Pour en savoir plus sur les [technologies employées dans NAKALA](https://documentation.huma-num.fr/media/nakala-technologies.png)  


## NAKALA et Huma-Num 

[NAKALA](https://www.huma-num.fr/les-services-par-etapes/#nakalapreservation) est un entrepôt de données développé par Huma-Num. L'entrepôt Nakala est basé sur des technologies éprouvées et respectant les standards (e.g. Framework Symfony, Triple Store GraphDB etc.). Une équipe de trois personnes de Huma-Num travaille sur son évolution et sa maintenance.

### Quel est le statut de Huma-Num ?

[Huma-Num](https://www.huma-num.fr/quest-ce-que-la-tgir-huma-num/) est une infrastructure nationale pour les Sciences Humaines et Sociales.  

En tant qu'insfrastructure nationale, Huma-Num est inscrite dans la [feuille de route nationale](https://www.enseignementsup-recherche.gouv.fr/cid70554/la-feuille-de-route-nationale-des-infrastructures-de-recherche.html) dont l'évolution est alignée sur celle des infrastructures europénnes gérée par l'[ESFRI](https://www.esfri.eu/). 

Huma-Num est opérée par le [CNRS](https://www.cnrs.fr) (Centre National de la Recherche Scientifique), une institution de recherche parmi les plus [importantes](https://www.cnrs.fr/fr/le-cnrs) au monde, qui a été créée en 1939. En 2021, le CNRS emploie plus de 30 000 personnes, dont plus de 10 000 chercheurs, et dispose d'un budget de 3 M€. 

### Comment est financée Huma-Num ?

Huma-Num est financée par le [MESRI](https://www.enseignementsup-recherche.gouv.fr/) (Ministère de l'Enseignement Supérieur, de la Recherche et de l'Innovation) dans le cadre de la feuille de route nationale des infrastructures (Cf. section précédente).


### Et si Huma-Num disparait ?

Comme Huma-Num est opérée par le CNRS, la responsabilité des données hébergées dans NAKALA lui est transférée en cas de dissolution de Huma-Num. 

Les choix technologiques effectués basés sur des standards internationaux permettront a minima de transférer les données pour les mettre à disposition avec les métadonnées associées sur un autre type d'infrastructure. Par exemple, les métadonnées étant exprimées dans le format [RDF](https://fr.wikipedia.org/wiki/Resource_Description_Framework), base des technologies du Web Sémantique, leur transfert dans un Triple-Store respectant ces standards sera simplifié. Les données elles-mêmes sont stockées sur des systèmes de fichiers standard (Unix). Les identifiants pérennes peuvent être mis à jour aisément pour conserver les liens d'accès aux données. 








