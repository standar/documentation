# Matomo : service d’analyse d’audience de sites web (statistiques d’usage pour sites web)

Huma-Num met à disposition une instance [Matomo](https://fr.matomo.org/) que vous pouvez utiliser pour analyser le trafic et l’usage de votre site web. Cette alternative à Google Analytics, vous permet de suivre l'usage de votre site web tout en conservant les données utilisateurs au sein du cloud Huma-Num.

Matomo est constitué d’une interface web qui vous permet de visualiser les usages selon divers indicateurs et d’[en configurer des nouveaux](https://fr.matomo.org/features/).

**Demander l’ouverture d’un compte Matomo** : la demande d’un compte Matomo se fait à partir de l’interface [HumanID](https://humanid.huma-num.fr). Pour cela, le cas échéant il est nécessaire de disposer d’un compte HumanID ([voir la documentation](humanid.md)). Pour demander l’accès au service il faut indiquer l’url du site web que vous souhaitez analyser.

L’accès au service Matomo hébergé par Huma-Num se fait sur  [analyseweb.huma-num.fr](https://analyseweb.huma-num.fr/).
