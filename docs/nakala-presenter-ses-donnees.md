---
lang: fr
---

!!! Note  
    Document en cours de rédaction


# Présenter les données stockées dans NAKALA et partager les métadonnées

NAKALA permet de séparer les dispositifs de stockage des données et ceux utilisés pour leur éditorialisation. En effet les technologies utilisées pour l'éditorialisation (ou la présentation) ont des cycles d’obsolescence relativement rapide et il existe un risque important « d’enterrer »  les données dans un dispositif. Un autre but est d’éviter la duplication des données dans plusieurs dispositifs.

## Composition d'une donnée dans NAKALA
Une donnée déposé dans NAKALA est composé essentiellement de deux éléments :  

- les métadonnées descriptives  
- un ou plusieurs fichiers associés

![Donnée dans NAKALA](media/nakala/nakala_donnee.png){: style="width:500px; border: 1px solid grey; display: block; margin-left: auto; margin-right: auto"}

*Composition d'une donnée dans NAKALA*

![Donnée dans NAKALA vue depuis l'interface](media/nakala/nakala_donnee_interface.png){: style="width:500px; border: 1px solid grey; display: block; margin-left: auto; margin-right: auto"}

*Présentation de ces éléments dans l'interface web de NAKALA*

## Indentifiants appliqués aux données 
Toute donnée déposé dans NAKALA dispose d'un identifiant :
- Les données déposées dans Nakala avant 2021 sont associées à des [Handles](http://handle.net/). 
Les Handles dans NAKALA sont de la forme  "11280/XXXXXXX". Les URLs associées sont les suivantes : 
https://nakala.fr/[handle] ou https://hdl.handle.net/[handle].

Par exemple :  
[https://nakala.fr/11280/111643de](https://nakala.fr/11280/111643de)  ou [https://hdl.handle.net/11280/111643de](https://hdl.handle.net/11280/111643de) 

- Les données déposées depuis 2021 sont associées à des [DOI](https://www.doi.org/) (Digital Object Identifiers). 
Les DOIs dans NAKALA sont de la forme "10.34847/nkl.XXXXXXXXXXX". Les URLs associées sont les suivantes :    
https://nakala.fr/[DOI] ou https://doi.org/[DOI]  

Par exemple :   
[https://nakala.fr/10.34847/nkl.4b33r2h4 ](https://nakala.fr/10.34847/nkl.4b33r2h4) ou [https://doi.org/10.34847/nkl.4b33r2h4](https://doi.org/10.34847/nkl.4b33r2h4) 

A ces identifiants sont associés des URLs spécifiques qui permettent l'accès aux différentes composantes de la donnée déposée :

- URL pour les métadonnées descriptives associées  
https://api.nakala.fr/datas/[identifiant]
- URL pour l'accès à un des fichiers de la donnée   
https://api.nakala.fr/data/[identifiant]/[identifiant_fichier]  
- URL pour accéder la visionneuse associée au fichier  
https://api.nakala.fr/embed/[identifiant]/[identifiant_fichier]  
- URL d'accès à un fichier via le protocole IIIF  
https://api.nakala.fr/iiif/[identifiant]/[identifiant_fichier]
- URL courte pour l'accès au premier fichier de la donnée
https://nakala.fr/data/[identifiant]


![URLs d'accès aux composants d'une donnée](media/nakala/nakala_url_composants.png){: style="width:500px; border: 1px solid grey; display: block; margin-left: auto; margin-right: auto"}

*URLs d'accès aux composants d'une donnée*

Ces URLs associées à une donnée sont accessibles depuis l'interface de NAKALA :

![URLs d'accès aux composants d'une donnée depuis l'interface](media/nakala/nakala_url_composants_interface.png){: style="width:500px; border: 1px solid grey; display: block; margin-left: auto; margin-right: auto"}

*URLs d'accès aux composants d'une donnée depuis l'interface web de NAKALA*

## Présenter les données stockées dans NAKALA


### Insertion de contenu dans du code HTML

Il est possible d'insérer des contenus issus de NAKALA dans une page web de tout type en entrant dans le code l'appel du fichier souhaité. Le langage HTML5 propose des balises adaptées à différents types de fichiers (e.g. images, vidéos, audio etc.)

On utilise alors l’identifiant du fichier (URL de téléchargement sur la page d’accueil) : https://api.nakala.fr/data/10.34847/nkl.4b33r2h4/e93c8f98abd8cf192e486408cfda3f6503cde84c

Voici quelques exemples d'intégration de données déposées dans NAKALA dans une page HTML. 

- Image 

```
    <img src="https://api.nakala.fr/data/[identifiant]/[identifiant_fichier]" alt="image de Nakala par exemple"/>
```

- Vidéo 

```
    <video src="https://api.nakala.fr/data/[identifiant]/[identifiant_fichier]" autoplay="true" preload="auto" controls></video>
```

- Audio 

```
    <audio src="https://api.nakala.fr/data/[identifiant]/[identifiant_fichier]" autoplay="true" controls></audio>
```


### Affichage du contenu avec la visionneuse intégrée de Nakala

Il est possible d'insérer dans ce contenu la visionneuse de Nakala.

NAKALA propose des visionneuses pour différents types de fichiers :    
- Fichier image : OpenSeadragon  
- Fichier CSV : DataTables  
- Fichier audio ou vidéo : Plyr  
- Fichier PDF : PDF.js  
- Fichier Markdown  
- Archives (.zip, .rar, .phar, .tar, .tgz, .gz, .bz2)
- Fichier de code (XML, HTML, JSON…)  

On utilise également l’identifiant du fichier (l'URL d’intégration est présente sur la page d’accueil) : https://api.nakala.fr/embed/10.34847/nkl.4b33r2h4/e93c8f98abd8cf192e486408cfda3f6503cde84c

L'intégration de la visionneuse se fait en utilisant la balise "<iframe\>"  

```
    <iframe src="https://api.nakala.fr/embed/[identifiant]/[identifiant_fichier]" width="hauteur" height="largeur"></iframe>
```

### API Image IIIF 

#### Implémentation du protocole IIIF dans Nakala 
NAKALA intègre le protocole [IIIF](https://iiif.io/) ((International Image Interoperability Framework).  

IIIF propose des standards d’interopérabilité en particulier pour le traitement et la manipulation d’images. 
Il est possible par exemple de :
- Sélectionner une partie de l’image (region)
- Modifier la taille de l’image (taille)
- Faire pivoter l’image (rotation)
- Choisir la qualité de l’image (qualite)
- Choisir le format (format)

Les extensions de fichiers images supportés par l'API Image IIIF de Nakala sont :

- tif
- tiff
- jpg
- jpeg
- jp2
- png
- pdf
- geojp2

#### Construction des URL pour utiliser IIIF Image
Ainsi pour manipuler une image dans NAKALA, l’URL se construit de la manière suivante :  
https://api.nakala.fr/iiif/[identifiant]/[identifiant_fichier]/{region}/{taille}/{rotation}/{qualité}.{format}.

Voici quelques exemples avec l’image [https://nakala.fr/11280/111643de](https://nakala.fr/11280/111643de).

- Modification de la taille de l’image  (paramètre {taille}) 
[https://api.nakala.fr/iiif/11280/111643de/b11ddf74a574b13ab04d790ebfd6d643db96eaed/full/200,200/0/default.jpg](https://api.nakala.fr/iiif/11280/111643de/b11ddf74a574b13ab04d790ebfd6d643db96eaed/full/200,200/0/default.jpg)

- Extraction d’une partie de l’image  (paramètre {région})  
[https://api.nakala.fr/iiif/11280/111643de/b11ddf74a574b13ab04d790ebfd6d643db96eaed/250,450,300,300/max/0/default.jpg](https://api.nakala.fr/iiif/11280/111643de/b11ddf74a574b13ab04d790ebfd6d643db96eaed/250,450,300,300/max/0/default.jpg)

- Rotation de l’image (paramètre {rotation})  
[https://api.nakala.fr/iiif/11280/111643de/b11ddf74a574b13ab04d790ebfd6d643db96eaed/full/max/45/default.jpg](https://api.nakala.fr/iiif/11280/111643de/b11ddf74a574b13ab04d790ebfd6d643db96eaed/full/max/45/default.jpg)

- Combinaison d’extraction, de rotation et de redimensionnement de l’image (paramètres [région], {taille} et {rotation})  
[https://api.nakala.fr/iiif/11280/111643de/b11ddf74a574b13ab04d790ebfd6d643db96eaed/250,650,400,300/300,300/45/default.jpg](https://api.nakala.fr/iiif/11280/111643de/b11ddf74a574b13ab04d790ebfd6d643db96eaed/250,650,400,300/300,300/45/default.jpg)


![Combinaison de plusieurs paramètres IIIF](media/nakala/nakala_iiif_multiple.png){: style="width:500px; border: 1px solid grey; display: block; margin-left: auto; margin-right: auto"}

*Combinaison de plusieurs paramètres IIIF dans un appel d'image déposée dans Nakala*


## Partager les métadonnées associées aux données 

Les métadonnées associées à un dépôt dans NAKALA sont accesssibles de différentes manières :

A partir de la page d’accueil d'une donnée :  
- Citation  
- Partage « visuel »  
- Métadonnées embarquées  dans la page d'accueil 

A partir de points d’accès utilisant des protocoles standardisés :  
- l'API de NAKALA  
- Le protocole OAI-PMH  
- Le Triple Store  


### A partir de la page d’accueil

#### Citation

A partir de l'interface, il est possible de récupérer une citation sous forme normalisée  

![Citation depuis l'interface](media/nakala/nakala_citation_interface.png)

#### Partage « visuel »

Les métadonnées complètes sont accessibles visuellement via l'interface

![Partage visuel depuisl'interface](media/nakala/nakala_partage_visuel_interface.png)

#### Métadonnées "embarquées" dans la page d'accueil  

Des métadonnées standardisées, destinées à être consommées par des machines, sont accessibles depuis la page d'accueil d'une donnée. 

![Métadonnées embarquées](media/nakala/nakala_metadonnees_embarquees.png)

Ces métadonnées peuvent par exemple être importées par le logiciel [Zotero](https://www.zotero.org/) 

![Métadonnées embarquées vues dans Zotero](media/nakala/nakala_metadonnees_embarquees_zotero.png)


### A partir de points d’accès standardisés  

#### API de NAKALA

Il est possible d'accéder aux métadonnées descriptive d'une donnée par l’[API](https://api.nakala.fr/doc)  de NAKALA  
https://api.nakala.fr/datas/[identifiant]

Par exemple  

[https://api.nakala.fr/datas/11280/111643de](https://api.nakala.fr/datas/11280/111643de  )   

Le format de sortie est en XML ou en Json récupérable de manière automatisée 

![Métadonnées récupérées via l'API](media/nakala/nakala_metadonnees_api.png)

Plus d'informations sur l'utilisation de l'API sont disponibles [ici](https://api.nakala.fr/doc) 

#### Protocole OAI-PMH

NAKALA propose un point d'accès utilisant le protocole [OAI-PMH](https://fr.wikipedia.org/wiki/Open_Archives_Initiative_Protocol_for_Metadata_Harvesting) 

Trois format de métadonnées sont disponibles :  
- Dublin Core  
- DCTerms  
- DataCite   

Les « SETs » (ensembles) OAI sont associés aux collections publiques de NAKALA. Chaque collection publique consitue donc un SET dans l'entrepôt OAI de NAKALA.

Le point d'accès aux métadonnées par le protocole OAI-PMH est de cette forme : https://api.nakala.fr/oai2?[paramètres du protocole OAI-PMH]

Voici un exemple avec la donnée utilisée plus haut :  
[https://api.nakala.fr/oai2?verb=GetRecord&identifier=oai:nakala.fr:hdl_11280_111643de&metadataPrefix=oai_dc](https://api.nakala.fr/oai2?verb=GetRecord&identifier=oai:nakala.fr:hdl_11280_111643de&metadataPrefix=oai_dc)  

![Métadonnées récupérées via le protocole OAI-PMH](media/nakala/nakala_metadonnees_oai.png){: style="width:500px; border: 1px solid grey; display: block; margin-left: auto; margin-right: auto"}

*Métadonnées récupérées via le protocole OAI-PMH*

#### Triple Store

!!! Note  
    Le Triple Store est en cours de mise à jour


### Les consommateurs de métadonnées 

#### ISIDORE 

Les collections publiques de NAKALA peuvent être "moissonnées" par [ISIDORE](https://isidore.science/). Ce moissonnage est facilité par la publication des collections dans l'entrepôt OAI.
Pour signaler une collection de NAKALA dans ISIDORE, il faut en faire la demande au support à isidore-sources@huma-num.fr. 
La [documentation](https://documentation.huma-num.fr/isidore/#comment-signaler-ses-donnees-dans-isidore-avec-des-metadonnees-et-le-protocole-oai-pmh) fournit des informations plus précises.

![Métadonnées vues par ISIDORE](media/nakala/nakala_metadonnees_isidore.png){: style="width:500px; border: 1px solid grey; display: block; margin-left: auto; margin-right: auto"}

*Affichage de métadonnées issues de NAKALA dans ISIDORE : [https://isidore.science/document/11280/111643de](https://isidore.science/document/11280/111643de)*


#### Gallica

Les collections de NAKALA peuvent être "moissonnées" par [GALLICA](https://gallica.bnf.fr) 

Le moissonnage n'est pas automatique, il est nécessaire d'en faire la [demande](https://www.bnf.fr/sites/default/files/2019-02/Guide_oaipmh.pdf) 

![Métadonnées vues par GALLICA](media/nakala/nakala_metadonnees_gallica.png){: style="width:500px; border: 1px solid grey; display: block; margin-left: auto; margin-right: auto"}

*Affichage de métadonnées issues de NAKALA dans Gallica*

#### Data Cite 

Les données de NAKALA possédant un identifiant DOI sont référencées par [DataCite](https://datacite.org/) 

Le moissonnage est automatique, au moment de la publication de la donnée (i.e de l'enregistrement de l'identifiant DOI)


![Métadonnées vues par DataCite](media/nakala/nakala_metadonnees_datacite.png)


#### OpenAire 

Les collections de NAKALA peuvent être "moissonnées" par [OpenAire](https://www.openaire.eu/) 

Le moissonnage n'est pas automatique, il est nécessaire d'en faire la demande 


!!! Note  
    Le lien avec OpenAire est en cours de mise à jour



#### Google Data Search 

Les collections de NAKALA peuvent être "moissonnées" par [Google Data Search](https://datasetsearch.research.google.com/)

Le moissonnage n'est pas automatique, il est nécessaire d'en faire la demande 

Google Data Search effectue un lien lorsque cela est possible entre  les données à les articles présents dans [Google Scholar](https://scholar.google.com/)


![Métadonnées vues par Google Data Search](media/nakala/nakala_metadonnees_googledatasearch.png)
