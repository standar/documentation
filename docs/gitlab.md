# GitLab : hébergement, versionning et partage de code

## Présentation
L’instance GitLab d’Huma-Num permet l’hébergement sécurisé et le partage maîtrisé de fichiers, dont des fichiers de code (tous langages) selon le protocole de versionnage _git_.

Il s’agit d’une implémentation du logiciel [Gitlab](https://about.gitlab.com/).

Les principales fonctionnalités sont la gestion de version et des dépôts (_git_), l’intégration continue, la génération de sites web statiques (sous la forme d'ensemble de pages), la gestion de tickets (_issues_).

## Publication des dépôts
Les dépôts publics créés dans l'instance GitLab d'Huma-Num sont diffusés sur la plateforme ["Codes sources du secteur public" Code.gouv.fr](https://code.gouv.fr) développée par [Etalab](https://etalab.gouv.fr) offrant une visibilité sur les codes sources des organisme publics français. Ils sont aussi archivés dans le cadre de [Software Heritage](https://www.softwareheritage.org).
La publication des dépôts sur code.gouv.fr implique leur mise sous [licence Ouverte 2.0](https://spdx.org/licenses/etalab-2.0.html).

## Fonctionnalités disponibles
Il est possible d'utiliser — avec l'instance GitLab proposée par Huma-Num, la fonctionnalité [GitLab _Pages_](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_ci_cd_template.html) pour générer et publier un site Web en HTML statique (dans le sous domaine huma-num.fr) par exemple avec des modèles de sites Web tel que HUGO, RTD, etc.

Gitlab _Pages_ utilisent Gitlab CI, c'est à dire le système d'intégration continue de Gitlab.

## Modalités de sauvegarde et stockage


## Ouverture d'un compte Gitlab
**Demander l’ouverture d’un compte Gitlab** : la demande d’un compte Gitlab se fait à partir de l’interface [HumanID](https://humanid.huma-num.fr). Pour cela, le cas échéant il est nécessaire de disposer d’un compte HumanID ([voir la documentation](humanid.md)).

Accès au service hébergé par Huma-Num : [gitlab.huma-num.fr](https://gitlab.huma-num.fr/).
