# Huma-Num Box : stockage sécurisé pour données tièdes et froides

## Description générale

Début 2016, Huma-Num a complété son offre de services par un dispositif
de stockage sécurisé distribué sur RENATER à destination principale de
ses structures partenaires, et consolidé sur ses points de présence à
Villeurbanne et à Paris.

Celui-ci vise à faciliter pour les chercheurs le
stockage, la sécurisation et la gestion de leurs jeux de données volumineux.

Le dispositif est indépendant des autres systèmes et services
d’Huma-Num (Sharedocs, NAKALA, hébergement Web et machine virtuelle).

Il offre un service complémentaire pour accueillir des jeux de données,
principalement de grandes tailles (plusieurs centaines de Téraoctets au total).

Ce dispositif est une infrastructure technique, indépendante des
usages fonctionnels (au sens des données : usage, référencement,
traitement, publication, ...) et des usages métier.

Il est à considérer comme un équivalent technique d’un serveur de fichiers ordinaire, avec
des fonctionnalités supplémentaires de sécurisation et de préservation
(cf. infra). Le dispositif utilise des disques magnétiques et des bandes
magnétiques, pour stocker les données.

## Caractéristiques des données pouvant être accueillies par le dispositif

Les données candidates à ce dispositif sont réputées "tièdes" voire
"froides", au sens où relativement peu d’accès en écriture et en lecture
seront effectués durant toute la vie de ces données.

Par contre ces données ont vocation à être conservées de manière fiable durant
plusieurs années (5 à 10 ans), car elles constituent la matière première
du travail de recherche et ont souvent une valeur de type
patrimonial.

Ce service de stockage peut être vu comme le pendant
numérique d’une armoire sécurisée où l’on stocke des documents
importants, nombreux, organisés (en rayonnages, boites archives,
dossiers) et documentés (fiches).
Ceci à la différence d’un bureau où se trouvent en vrac les documents courants et de toutes natures.

Ces données sont notamment issues de campagnes de numérisation de fonds
anciens, de photos, d’enregistrements audio, de cartes, de vidéos, de
modèles 3D. Elles existent uniquement sous la forme de fichiers,
éventuellement accompagnés de fichiers de méta-données techniques et
documentaires librement produits par les utilisateurs.

Le dispositif lui-même ne gère pas l’association des méta-données et des données&nbsp;:
c’est à l’utilisateur de prendre en charge cet aspect.

Les données stockées ne peuvent pas être des bases de données au sens informatique.
Seuls des fichiers peuvent être stockés sur le dispositif.

Leur volume pourra atteindre plusieurs Téraoctets par jeu de
données. Les données sont organisées en "partages" (ou "volumes"),
disposant de règles d’accès comme celles utilisées sur des services de
fichiers classiques.

Chaque gestionnaire de ces partages définit
librement l’organisation arborescente des fichiers contenus dans chaque
partage, afin de répondre au plan de classement qu’il aura défini
lui-même.

## Caractéristiques fonctionnelles du dispositif

Pour chaque partage, il est défini :

- les personnes pouvant accéder en lecture et en écriture à ces données ;
- les sites et plus finement les adresses IP pouvant accéder à ces données (un même partage pourra être accessible depuis plusieurs sites) ;
- le nombre de copies des données pouvant varier typiquement de 1 à 2 ;
- avec ou non une copie sur bandes magnétiques ;
- la gestion des versions dans le temps d’un même fichier (mécanisme d’historisation) ;
- la durée de rétention permettant de conserver des données supprimées par les utilisateurs (mais restant accessibles aux administrateurs du dispositif).

Globalement, chaque partage est caractérisé par une politique d’accès
et une politique de sécurisation qui lui sont propres, et qui peuvent
être modifiées au fil du temps.

Le dispositif assure en continu le
respect de ces politiques. Ainsi la modification des localisations, du
nombre d’instances, du nombre de versions dans le temps, ne nécessite
aucune opération humaine.

## Architecture du dispositif

Le dispositif est à considérer comme un ensemble intégré unique,
comportant plusieurs serveurs répartis entre les structures partenaires et Huma-Num.

À ce jour, les structures partenaires sont les MSH de
Rennes, Nantes, Val de Loire, Toulouse et Lyon, ainsi que l’EHESS
(Paris) et La Contemporaine (Nanterre).

10 serveurs sont en production
pour une capacité de 800 Téraoctets sur disques et 1000 Téraoctets sur bandes.

Près de 200 jeux de données sont définis, pour environ 650 Téraoctets.

Chaque serveur a un rôle équivalent aux autres et participe aux divers traitements
mis en œuvre, notamment pour la sécurisation constante des données.

Le dispositif maintient un catalogue global et unique des partages et des données,
dont chaque partie est présente sur au moins trois serveurs, afin de la sécuriser fortement. 

L’authentification des utilisateurs s’appuie sur l’annuaire
LDAP/Human-ID d’Huma-Num commun à de nombreux services de la TGIR.

La gestion de l’annuaire est déléguée par branche à chaque site participant grâce à
l’outil FusionDirectory et l’interface accessible en
[annuaire.huma-num.fr](https://annuaire.huma-num.fr).

Enfin la volumétrie disponible et les licences logicielles sont également globales.
On parle de stockage virtualisé.

L’investissement d’Huma-Num consiste en :

-   les 10 serveurs et les 14 baies de stockage de marque Dell ;
-   les licences du logiciel Active-Circle de la société Oodrive ;
-   une garantie matérielle et logicielle sur 7 ans sur l’ensemble;
-   la ressource humaine pour la gestion complète du dispositif.

Huma-Num est le seul maître d’œuvre et administrateur du dispositif.
Chaque projet demande à Huma-Num la création, la suppression
ou la modification des caractéristiques des partages.

Un groupe d’utilisateurs est associé à un partage, permettant ainsi de déléguer
la gestion de qui accède au partage (par gestion de ce groupe dans l’annuaire commun).

Des présentations du dispositif sont disponibles en :

-   [Rencontre Huma-Num 2018](https://rhn2018.sciencesconf.org/data/20180612_hnbox.pdf)
-   [Conférence JRES 2017](https://isidore.science/document/10670/1.yd8n65)
-   Les documentations de l’éditeur sont en [activecircle-help.com/](https://activecircle-help.com/fr/%C2%A0).

## Accès au dispositif

Il est à noter qu’il n’est pas du tout nécessaire de se
trouver sur un site où se trouve un des noeuds pour utiliser le service.
Celui-ci est accessible à tout le monde, sans prérequis technique ou
d’hébergement de serveur ou encore d’investissement financier.

Huma-Num se réserve la responsabilité de faire évoluer l’architecture du
dispositif en fonction de sa croissance et de l’opportunité de
positionner des noeuds supplémentaires dans tel ou tel site.

### A. Depuis tout point de l’Internet

Trois méthodes d’accès sont disponibles :

1.   via un client utilisant le protocole SFTP (comme Filezilla, rsync, etc.) vers le serveur sftp.huma-num.fr
2.   via une interface Web en lecture uniquement permettant une consultation minimaliste de l’arborescence des fichiers (URL à choisir). Un exemple est en [images.eurhisfirm.eu](http://images.eurhisfirm.eu) ;
3.  de plus, il est tout à fait possible de coupler une application Web plus élaborée en lien avec un jeu de données stocké dans le dispositif. Les sites Cocoon, Archeogrid, Telemeta illustrent cette possibilité.

### B. Pour les sites hébergeant un des noeuds du dispositif

L’intérêt secondaire du service est aussi d’amener une fonction de type
"serveur de fichiers" au plus près des utilisateurs sur le réseau
local où se trouve un des noeuds.
Ceci afin d’en rendre l’usage le plus simple possible, proche de celui d’un disque interne.

L’accès aux partages peut alors se faire par une fonction «Connexion à un lecteur réseau»
Le jeu de données apparaît sur le Bureau ou dans l’Explorateur de
fichiers de l’ordinateur de l’utilisateur.
Il peut alors manipuler les données strictement comme si elles étaient sur son disque interne.

Pour étendre cette fonctionnalité
aux utilisateurs ne se trouvant pas sur un site où est hébergé un noeud,
un dispositif de VPN (Virtual Private Network) est disponible.

## Limitations techniques du dispositif

### Limitations sur le nombre de fichiers

Comme tout système de stockage distribué, le dispositif est sensible au
nombre de fichiers. Aussi chaque structure candidate indique un ordre de
grandeur du nombre de fichiers qu’elle compte déposer.

Au delà de 10 millions de fichiers, une étude devra être menée avec Huma-Num pour
préciser la faisabilité.
De plus, il est instamment demandé de ne pas dépasser environ 10&nbsp;000 fichiers dans un même dossier.
Au-delà, il convient d’étudier avec Huma-Num les solutions de contournement.

### Limitations sur la nature des données

Les données sous forme de bases de données binaires (MySQL, PostgreSQL,
etc.) ne peuvent pas être stockées dans le dispositif. De même pour les
disques virtuels de machines virtuelles.

## Performances

Le dispositif n’est pas conçu pour :

-   un temps d’accès ultra-rapide ;
-   une très grande vitesse en transfert, en lecture comme en écriture ;
-   un usage bureautique ;
-   un traitement intensif et parallèle sur les données.


Néanmoins le retour d’expériences après cinq d’années d’exploitation
est positif. Aucune limitation perceptible par les utilisateurs du
dispositif n’est à noter.

Des vitesses de transfert de plus de 50 Mo/s (500 Mb/s)
sont observées régulièrement.

Des traitements intensifs ont pu être réalisés avec de bonnes performances,
en respectant un traitement séquentiel.

De plus, trois sites Web importants publient
désormais leurs fichiers media via un stockage sur ce dispositif. Il
s’agit des sites Cocoon, Archeogrid et Telemeta.

Aussi il convient de noter que le dispositif peut être utilisé
pour publier un corpus de données par une application Web sous forme
de fichiers de taille significative (images, sons, vidéos).
Ainsi une instance Omeka-S et une instance de serveur IIIF sont reliées au dispositif.

## Disponibilité, intégrité, confidentialité

L’engagement d’Huma-Num sur ce dispositif est de la même nature que
pour les autres services :

- "best effort", quant à la disponibilité ;
- haut niveau, quant à l’intégrité et la confidentialité.

Le retour d’expérience sur cinq ans montre :

- aucun incident concernant la conservation, l’intégrité ou la confidentialité des données ;
- une disponibilité très satisfaisante.

Le dispositif est réputé sûr quant à la conservation des données,
moyennant évidemment l’existence de deux jeux de données sur deux
sites. Un mécanisme de vérification automatique des signatures des
fichiers peut être mis en place.

De plus, un archivage binaire des données peut être mis en place sur
bandes magnétiques LTO. Ceci constitue une duplication des données sur
d’autres médias que des disques magnétiques. Ces bandes peuvent être
verrouillées en écriture, sont relues et contrôlées automatiquement et
périodiquement, et elles sont écrites dans un format ouvert et normalisé
(TAR).

Quant à la confidentialité, plusieurs points :

-   Elle est d’abord garantie par la bonne gestion des droits d’accès aux partages.
-   Les accès en SFTP opèrent un chiffrement de la communication entre l’utilisateur et le service.
-   Une fonction de chiffrement à la volée est désormais disponible, qui assure une conservation chiffrée sur disques et bandes.
-   Une journalisation exhaustive des accès aux données est en place.
-   Le besoin de chiffrement des données sur les PC des utilisateurs reste à leur charge.


## Support

Toute demande concernant ce service doit impérativement être envoyée à l’adresse [assistance@huma-num.fr](mailto:assistance@huma-num.fr).
