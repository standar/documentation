description: Cette documentation concerne le service d'hébergement web mutualisé

# Hébergement Web mutualisé : Linux, Apache, MySQL/PostgreSQL, PHP, Java

## Conditions Générales d’Utilisation (CGU)

Voir dans cette documentation, [les CGU](cgu.md).

## Référence à Huma-Num

Vous êtes invités à mentionner sur la page d’accueil de votre site Web
le fait que vous êtes hébergé par la TGIR Huma-Num.

Vous trouverez un choix
d’images sur [www.huma-num.fr/supports-communication/](https://www.huma-num.fr/supports-communication/).

## Annuaire des sites hébergés

Un annuaire des sites Web hébergés est consultable sur [www.huma-num.fr/annuaire-des-sites-web](https://www.huma-num.fr/annuaire-des-sites-web).

Chaque gestionnaire de site est invité à demander l’inscription et la modification de son site
dans cet annuaire.

## Interopérabilité et signalement des données

Il est rappelé qu’un des critères essentiels d’hébergement est la mise
en place d’une interopérabilité des métadonnées et données, afin
qu’elles soient signalées dans notre plateforme [ISIDORE](https://isidore.science/).

Concernant les données, nous vous encourageons à adopter les bonnes pratiques en terme de formats (pérennité) en vous basant sur la liste proposée par l’outil [FACILE du CINES](https://facile.cines.fr/).

Dans le cadre des corpus de données utilisant des bases de données, nous
souhaitons que les projets s’inscrivent dans le signalement de leurs
données par l’intermédiaire de métadonnées normalisées et moissonnables
selon le protocole OAI-PMH par notre moteur de recherche ISIDORE.

Le protocole OAI-PMH permet en outre un moissonnage par d’autres
institutions (Gallica, Europeana) ou par des portails web
disciplinaires. Il s’agit du protocole documentaire le plus
répandu actuellement dans le monde des humanités numériques.

Pour cela, un [guide d’implémentation des méthodes de moissonnage](isidore.md) est
disponible sur notre site.

Les demandes de moissonnage ou d’aide pour sa mise en place sont à adresser à
[isidore-sources@huma-num.fr](mailto:isidore-sources@huma-num.fr).

## Ce que la TGIR ne fait pas

Un élément important est à noter : l’équipe de la TGIR Huma-Num ne peut
se substituer aux gestionnaires des sites pour développer, maintenir des
applications développées dans les équipes de recherche (ou par des prestataires de
services). Nous ne développons pas de logiciels propres pour les
programmes de recherche.

Ainsi, les équipes de recherche qui n’ont pas d’informaticien
(développeurs, etc.) sont invitées à utiliser nos solutions "clés en
main" : NAKALA, NAKALA-PRESS, ISIDORE ou bien sûr à constituer des
programmes de recherche embarquant des développeurs. Vous pouvez solliciter l’équipe Huma-Num pour tout avis et conseil en amont de la construction du
programme.

## Ce que le gestionnaire du site doit faire : engagement de mise à jour

Chaque gestionnaire de site porte la responsabilité de réaliser, durant
toute la vie du site, la mise à jour continue des briques applicatives qu’il aura installées.

Ainsi une mise à jour très rapide des CMS (Content Management System) les plus courants
comme Wordpress, Drupal, Joomla, SPIP, devra impérativement être réalisée à chaque publication
d’une nouvelle version ou d’un avis de sécurité.

## Serveurs

Nos serveurs sont sous le système d’exploitation CentOS 7.

Seul un serveur interactif vous est accessible par SSH et SFTP.

Les serveurs Web, d’applications et de bases de données ne vous sont pas accessibles
de manière interactive.

Mais votre dossier principal ("home directory") est le même entre tous les serveurs concernés.
Aussi vous avez la vue et la main sur l’ensemble de vos fichiers.

## Langages, logiciels et applications disponibles

Les langages et logiciels mis en œuvre dans le cadre de l’hébergement Web mutualisé :

-   Langage de programmation : PHP
-   Logiciels de bases de données relationnelles : MySQL, PostgreSQL (Extension PostGIS)
-   Logiciels et bases de données XML : BaseX, eXist
-   Serveur d’applications : Tomcat
-   Moteur de recherche : SolR

Ainsi une grande partie des applications Web couramment utilisées sont hébergeables sur ce service.

## Langage de script PHP

La version 7.4 de PHP est proposée par défaut. Un fichier contenant `<?php phpinfo(); ?>` vous permet de voir la version utilisée par votre site et les modules disponibles.

Le fichier	`resource/wrapper/php74/php.ini` est lu spécifiquement pour votre site Web. Vous pouvez le modifier si besoin pour modifier des paramètres PHP.

## Serveurs MySQL et PostgreSQL

Le serveur MySQL en version 8 et se nomme `mysql.db.huma-num.fr`.

Le serveur PostgreSQL 11 et se nomme `postgresql.db.huma-num.fr`.

Sur ces 2 instances, **le mode SSL est activé**.

Une interface de gestion de vos bases de donnée MySQL est disponible à l’aide du logiciel en ligne PHPMyAdmin
et accessible à l’adresse [mygrid.huma-num.fr/tools/myadmin_v4/](https://mygrid.huma-num.fr/tools/myadmin_v4/).

## Applications Java

Il est possible d’ajouter une application basée sur un serveur d’applications Java à son site HTML ou PHP.

Cependant il convient de demander à l’équipe Huma-Num de réaliser l’installation de cette application.

L’application se trouvera en `resource/<application>/current` où `application` peut donc valoir `tomcat`, `exist`, `elasticsearch`, `solr`, `basex` ou `virtuoso`.

Pour passer un ordre à cette application, il convient :

-   d’écrire l’ordre dans le fichier texte nommé `resource/order/<application>` où l’ordre peut valoir `stop`, `start`, `restart`, `kill`, `forcekill`, `top` ;
-   et d’attendre maximum 3 minutes pour voir le résultat dans `resource/order/<application>.out`

## Envoi de messages électroniques

Vous disposez d’un serveur SMTP permettant l’envoi de messages à l’adresse `relay.huma-num.fr` (ceci sans chiffrement, sans authentification et sur le port 25 traditionnel).

## Journaux

Les journaux Apache sont stockés jour par jour et conservés pendant un an dans votre dossier `www/log`.

## Statistiques Web

Nous proposons un service de statistiques sur les consultations basé sur l’outil Piwik/Matomo.

Il convient d’en faire la demande sur [humanid.huma-num.fr](https://humanid.huma-num.fr/).

## Sauvegardes

Une sauvegarde incrémentale de l’ensemble de vos fichiers et de vos bases de données est effectuée chaque nuit.

Vous avez un accès direct aux "snapshots" (photos instantanées) prises par le serveur de fichiers à intervalle régulier. La commande suivante vous permet de lister les dates des snapshots disponibles :

```bash
	ls -l .snapshot
```

Vous pouvez ensuite parcourir ces dossiers de manière usuelle, pour récupérer un fichier ou un dossier dans leur version précédente.

## Support

Toute demande concernant ce service doit impérativement être envoyée à l’adresse [assistance@huma-num.fr](mailto:assistance@huma-num.fr)

## Machine virtuelle

Dans le cas où :

-  vous avez des bonnes raisons de penser que des limitations techniques existent sur cet hébergement Web mutualisé,
-  ou si vous souhaitez plus d’autonomie de gestion technique tout en pouvant assumer l’administration système d’un système Linux,

nous pouvons mettre à disposition une machine virtuelle dans laquelle vous disposez du droit d’administration.
