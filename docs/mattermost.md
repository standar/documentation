# Mattermost - Service de Chat (discussion en ligne)

Huma-Num met à disposition une instance [Mattermost](https://mattermost.com/), service de chat (discussion instantannée)
interne et sécurisé pour les organisations et les entreprises.

Mattermost permet de partager des messages et des fichiers sur différentes plateformes notamment, les PCs, les
téléphones et les tablettes. Le stockage continu et la recherche instantanée, et prend en charge les notifications et
les intégrations avec vos outils existants.

**Demander l’ouverture d’un compte Mattermost** : la demande d’un compte Mattermost se fait à partir de
l’interface [HumanID](https://humanid.huma-num.fr). Pour cela, le cas échéant il est nécessaire de disposer d’un compte
HumanID ([voir la documentation](humanid.md)).

Vous pouvez initier vous-même un canal de discussion ou en rejoindre un déjà créé. Lors de la demande de ce service sur HumanID, il faut renseigner votre
établissement de rattachement et les équipes à joindre ou à créer.

!!! Avertissement
    Mattermost étant un service de discussion instantannée, nous attirons l'attention de nos utilisateurs sur le fait que ça n'en fait pas un outil de stockage/sauvegarde de données de la recherche. Une fois le projet terminé ou finalisé, le canal de discussion pourra être supprimé entraînant la perte des informations concernées. Dans le cadre d'un projet de recherche il est conseillé de bien encadrer l'utilisation de ce service uniquement pour de la discussion.

Accès au service hébergé par Huma-Num : [chatting.huma-num.fr](https://chatting.huma-num.fr/)
