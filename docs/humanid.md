# HumanID : Connexion centralisée aux services d’Huma-Num

HumanID ([humanid.huma-num.fr](https://humanid.huma-num.fr/)) est une interface d’authentification, basée sur le
logiciel [LemonLDAP::NG 2.0](https://lemonldap-ng.org/welcome/), qui permet de retrouver sur une seule interface les
services Web d’Huma-Num que vous avez ouvert ou que vous pouvez ouvrir.

![humanid_services](media/humanid/services.png){: style="width:600px; border: 1px solid grey;"}

Pour l’ouverture de services non présents sur cette page, votre demande se fait par e-mail à l’adresse : [cogrid@huma-num.fr](mailto:cogrid@huma-num.fr).

## Créer un compte HumanID

Rendez-vous sur [humanid.huma-num.fr](https://humanid.huma-num.fr/) :

- Onglet orange première visite sur Huma-Num &rarr; Créer un compte HumanID.

![humanid_create_account](media/humanid/account.png){: style="width:450px;"}

- Vous indiquez dans le formulaire vos nom, prénom et mail
- Vous recevez un mail de confirmation : `[HumanID] Confirmation d’enregistrement de compte`
- Puis un mail de confirmation de création de compte : `[HumanID] Votre nouveau compte`

Une fois votre compte créé, en allant sur l’onglet `Mes identifiants`, vous pouvez connecter votre compte à des comptes
externes (Fédération de Recherche eduGAIN, ORCID, HAL, LinkedIn, Twitter et Google). Remarque : vous pouvez également
vous connecter à HumanID lors de votre première visite grace à ces identifiants externes ; un compte HumanID sera alors
créé automatiquement si aucun compte n'existe déjà.

![humanid_extID](media/humanid/extid.png){: style="width:500px; border: 1px solid grey;"}

## Tutoriels
- HumanID : [Vidéo de présentation du service HumanID](https://documentation.huma-num.fr/media/humanid/HumanidTuto.mp4) (mp4 - 60 MO)

- Présentation en 5 étapes : [comment créer votre compte HumanID](media/humanid/TutoCreationHumanID_24112021.pdf) (pdf - 133 ko)

## Questions et Contact

Pour tout problème ou question liée à l’utilisation de HumanID, veuillez envoyer un mail à l’adresse suivante :
[assistance@huma-num.fr](mailto:assistance@huma-num.fr).
