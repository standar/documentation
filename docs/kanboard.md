---
title: Kanboard
lang: fr
description: Kanboard, service de gestion de projet basé sur la méthode Kanban
---

# Kanboard

Kanboard est un service de gestion de projet basé sur la méthode [Kanban](https://fr.wikipedia.org/wiki/Kanban_(d%C3%A9veloppement))

Huma-Num met à disposition une instance Kanboard que vous pouvez utiliser pour la gestion de votre projet de recherche.
Kanboard est un logiciel de gestion de projet offrant diverses fonctionnalités :

- création et organisation visuelle des tâches (système de cartes)

- gestion des tâches (glisser/déposer) 

- attribution des tâches, dates d'échéances

- etc.

!!! Ressources
    - [Documentation utilisateur](https://docs.kanboard.org/fr/latest/index.html) de Kanboard
    - [Site web](https://kanboard.org) officiel du projet Kanboard


Demander l’ouverture d’un compte Kanboard : la demande d’un compte Kanboard se fait à partir de l’interface [HumanID](https://humanid.huma-num.fr). Pour cela, au préalable, il est nécessaire de disposer d'un compte HumanID ou d'en créer un ([voir la documentation](humanid.md)).

L’accès au Kanboard hébergé par Huma-Num se fait sur [kanboard.huma-num.fr](http://kanboard.huma-num.fr/).
