
#Page.hn : générateur d'URL courtes

## Présentation

[Page.hn](https://page.hn) est un service en ligne, multilingue, permettant de créer des URL courtes. Il est développé et maintenu par la [TGIR Huma-Num](https://www.huma-num.fr) pour l'ensemble de la communauté d'enseignement et de recherche des sciences humaines et sociales. Si la création d’une URL courte dans Page.hn se fait après connexion via [HumanID](https://humanid.huma-num.fr), l’utilisation d’une URL courte générée est publique, libre et ouverte.

Page.hn répond au double besoin de respect des données personnelles et d'indépendance vis-à-vis des outils de sociétés privées qui peuvent être utilisés pour générer des pourriels par exemple.

Les URL courtes fournies par page.hn sont en HTTPS et de la forme : page.hn/[identifiant].

## Limitations

Page.hn n'est pas un service d'identifiants pérennes contrairement aux dispositifs réservés aux articles ou aux jeux de données (par exemple les DOI dans [NAKALA](https://nakala.fr) ou les identifiants de type Handle ou les DOI et identifiants de type ARK réexposés dans [ISIDORE](https://isidore.science). Pouvant être appliqués sur tout type d'URL, y compris certains lié a des évènements ponctuels, ces URL n'ont pas vocation a durer dans le temps.

Page.hn ne garantit pas la pérennité de la page Web liée à l'URL courte. Par ailleurs, ce service ne détecte pas l'absence soudaine ou momentanée d'une URL raccourcie.

Enfin, Page.hn ne permet pas de passer des paramètres derrière l'URL courte. Mais elle peut encapsuler les paramètres d'une URL longue dont l'éditeur souhaite un raccourci.

## Fonctionnement

La page principale de Page.hn fonctionne tel un tableau de bord qui vous offre la possibilité de lister les URL courtes déjà créées, mais aussi d'en gérer l'édition (ajout, edition/modification, suppression).

![PageHN](media/page-hn-accueil-principal.png){: style="width:75%; border: 1px solid grey;"}

*Page principale du service Page.hn*

### Description

- URL : Cela correspond à l'identifiant de l'URL courte. Elle est de la forme page.hn/[identifiant]. Il est possible de copier directement le lien généré en utilisant le petit pictogramme attenant ;
- URL d'origine : Il s'agit de l'URL d'origine, celle que vous souhaitez raccourcir ;
- Date : C'est la date de création de l'URL ou celle de la dernière modification ;
- Les pictogrammes suivants permettent de :
 - [i] : Affiche les métadonnées de l'URL courte ;
 - [stylo] : Permet d'éditer l'URL d'origine ;
 - [poubelle] : Permet de supprimer une URL courte et ses métadonnées attenantes.
- [+] : Permet de créer une nouvelle URL courte
