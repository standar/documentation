---
title: OpenTheso
lang: fr
description: OpenTheso, un service tiers hébergé par Huma-Num et lien vers sa documentation.
---

# OpenTheso

!!! attention "À savoir"
    Les services tiers sont des outils et des plateformes dont la conception,
    la maintenance et la documentation sont assurés par  d'autres instances ou
    organisations que la TGIR Huma-Num. La TGIR Huma-Num met à disposition des
    machines virtuelles hébergées sur son infrastructure pour assurer l'accessibilité
    de ces outils et soutenir leur usage par la communauté SHS.


_Opentheso_ est un logiciel libre de gestion de thésaurus multilingue. Il peut être utilisé dans divers contextes : bases de données, système de gestion de bibliothèque, description de fonds d'archives, gestion bibliographique, système d'information géographique[^2].

_Opentheso_ est une solution collaborative de construction et de gestion de terminologie spécialisée ou de vocabulaire contrôlé. Il assure l'interopérabilité des terminologies utilisées (identifiants pérennes, conformité aux normes ISO et au modèle RDF).

Il est développé par la plateforme technologique [WST](https://www.mom.fr/plateformes-technologiques/web-semantique-et-thesauri) (Web Sémantique & Thesauri) située à la [Maison de l'Orient et de la Méditérranée](https://www.mom.fr/) de Lyon en partenariat avec le [GDS-FRANTIQ](http://www.frantiq.fr/).

**Demande d'ouverture d'un compte compte sur OpenThéso :** les demandes sont à adresser à [cogrid@huma-num.fr](mailto:cogrid@huma-num.fr) en précisant le contexte : projet, type de thésaurus.

[^2]: Source [Wikipédia](https://fr.wikipedia.org/wiki/Opentheso)

!!! note
    - Voir l'instance _Opentheso_ hébergée par la TGIR Huma-Num : [opentheso.huma-num.fr](https://opentheso.huma-num.fr/opentheso/)
    - Voir la documentation d'_Opentheso_ : [https://opentheso.hypotheses.org](https://opentheso.hypotheses.org)
    - Voir la présentation sur le site du Consortium MASA : [masa.hypotheses.org/opentheso](https://masa.hypotheses.org/opentheso)
